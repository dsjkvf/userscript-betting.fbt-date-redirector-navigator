// ==UserScript==
// @name        betting2: FBT date redirector
// @namespace   oyvey
// @homepage    https://bitbucket.org/dsjkvf/userscript-betting2-fbt-date-redirector-navigator
// @downloadURL https://bitbucket.org/dsjkvf/userscript-betting2-fbt-date-redirector-navigator/raw/master/fbtrn.user.js
// @updateURL   https://bitbucket.org/dsjkvf/userscript-betting2-fbt-date-redirector-navigator/raw/master/fbtrn.user.js
// @match       *://*.footballbettingtips.org/*
// @run-at      document-start
// @version     1.0.6
// @grant       none
// ==/UserScript==


// INIT

var url = window.location.href;
var urlLastPart = url.substr(url.lastIndexOf('/') + 1);


// HELPERS

function redirectToDate(url, delta = 0) {
    var parts = [];
    if (/html$/.test(url)) {
        parts = urlLastPart.split(/[-.]+/);
        parts[2] = Number(parts[2]) + delta;
        if (parts[2] < 10) {parts[2] = '0' + parts[2]};
    } else {
        var today = new Date();
        parts[2] = today.getDate();
        if (parts[2] < 10) {parts[2] = '0' + parts[2]};
        parts[1] = today.getMonth() + 1; //January is 0!
        if (parts[1] < 10) {parts[1] = '0' + parts[1]};
        parts[0] = today.getFullYear();
    }

    var datePage = parts[0] + '-' + parts[1] + '-' + parts[2] + '.html';
    return url.split('/').slice(0, 3).join('/') + '/tips/' + datePage
}

// MAIN

// Immediately redirect to today if an url doesn't contain a link to a date
if (! /html$/.test(url)) {
    window.location.replace(redirectToDate(url));
}

// Add hotkeys to navigate tips pages back an forth
addEventListener('keyup', function(e) {
    if (e.ctrlKey ||
        e.altKey ||
        e.metaKey ||
        e.shiftKey ||
        (e.which !== 74 /*j*/ && e.which !== 75 /*k*/)) {
        return;
    }
    e.preventDefault();

    if (e.which == 74) {
        window.location.replace(redirectToDate(url, -1));
    } else if (e.which == 75) {
        window.location.replace(redirectToDate(url, +1));
    }
})
